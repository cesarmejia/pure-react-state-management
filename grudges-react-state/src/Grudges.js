import React, { memo } from 'react';
import Grudge from './Grudge';

const Grudges = ({ grudges = [], dispatch }) => {
  return (
    <section className="Grudges">
      <h2>Grudges ({grudges.length})</h2>
      {grudges.map(grudge => (
        <Grudge key={grudge.id} grudge={grudge} dispatch={dispatch} />
      ))}
    </section>
  );
};

export default Grudges;
