import React, { useReducer } from 'react';

import Grudges from './Grudges';
import NewGrudge from './NewGrudge';

import initialState from './initialState';

export const GRUDGE_ADD = 'GRUDGE_ADD';
export const GRUDGE_FORGIVE = 'GRUDGE_FORGIVE';

const reducer = (state, action) => {
  switch (action.type) {
    case GRUDGE_ADD:
      return [action.payload, ...state];
    case GRUDGE_FORGIVE:
      return state.map(grudge => {
        if (grudge.id !== action.payload.id) {
          return grudge;
        }
        return { ...grudge, forgiven: !grudge.forgiven };
      });
    default:
      return state;
  }
};

const Application = () => {
  const [grudges, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="Application">
      <NewGrudge dispatch={dispatch} />
      <Grudges grudges={grudges} dispatch={dispatch} />
    </div>
  );
};

export default Application;
