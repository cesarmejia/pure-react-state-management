import React, { memo } from 'react';
import { GRUDGE_FORGIVE } from './Application';

const Grudge = memo(({ grudge, dispatch }) => {
  const handleChange = () =>
    dispatch({ type: GRUDGE_FORGIVE, payload: { id: grudge.id } });

  return (
    <article className="Grudge">
      <h3>{grudge.person}</h3>
      <p>{grudge.reason}</p>
      <div className="Grudge-controls">
        <label className="Grudge-forgiven">
          <input
            type="checkbox"
            checked={grudge.forgiven}
            onChange={handleChange}
          />{' '}
          Forgiven
        </label>
      </div>
    </article>
  );
});

export default Grudge;
