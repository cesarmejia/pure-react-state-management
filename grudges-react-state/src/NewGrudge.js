import React, { useState, memo } from 'react';
import id from 'uuid/v4';
import { GRUDGE_ADD } from './Application';

const NewGrudge = memo(({ dispatch }) => {
  const [person, setPerson] = useState('');
  const [reason, setReason] = useState('');

  const addGrudge = () => {
    const grudge = {
      id: id(),
      forgiven: false,
      person,
      reason
    };
    return { type: GRUDGE_ADD, payload: grudge };
  };

  const handleSubmit = event => {
    event.preventDefault();
    dispatch(addGrudge());
  };

  return (
    <form className="NewGrudge" onSubmit={handleSubmit}>
      <input
        className="NewGrudge-input"
        placeholder="Person"
        type="text"
        value={person}
        onChange={event => setPerson(event.target.value)}
      />
      <input
        className="NewGrudge-input"
        placeholder="Reason"
        type="text"
        value={reason}
        onChange={event => setReason(event.target.value)}
      />
      <input className="NewGrudge-submit button" type="submit" />
    </form>
  );
});

export default NewGrudge;
