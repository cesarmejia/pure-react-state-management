# Pure React State Management

Code follow along to Steve Kinney's (FEM) Pure React State Management v2 Workshop

## Table of Contents

- [Pure React State Management](#pure-react-state-management)
  - [Table of Contents](#table-of-contents)
  - [Checklist](#checklist)
    - [Introduction](#introduction)
    - [Class-Based State](#class-based-state)
    - [Hooks State](#hooks-state)
    - [Reducers](#reducers)
    - [Context](#context)
    - [Data Fetching](#data-fetching)
    - [Thunks](#thunks)
    - [Wrapping Up](#wrapping-up)
  - [Notes](#notes)

## Checklist

### Introduction

- [x] ~~_Introduction_~~ [2020-03-01]
- [x] ~~_Types of State_~~ [2020-03-01]

### Class-Based State

- [x] ~~_setState & Class_~~ [2020-03-01]
- [x] ~~_setState & Asynchronicity_~~ [2020-03-01]
- [x] ~~_setState & Function_~~ [2020-03-01]
- [x] ~~_setState & Callback_~~ [2020-03-01]
- [x] ~~_setState & Helper Function_~~ [2020-03-01]
- [x] ~~_document.title Exercise_~~ [2020-03-01]
- [x] ~~_setState Patterns_~~ [2020-03-01]

### Hooks State

- [x] ~~_Refactoring & Hooks_~~ [2020-03-01]
- [x] ~~_useEffect & Dependencies_~~ [2020-03-01]
- [x] ~~_useEffect Exercise_~~ [2020-03-01]
- [x] ~~_useEffect Solution_~~ [2020-03-01]
- [x] ~~_Refactoring & Custom Hook_~~ [2020-03-01]
- [x] ~~_Persisting State & useRef_~~ [2020-03-01]
- [x] ~~_useEffect & Cleanup_~~ [2020-03-01]

### Reducers

- [X] ~~*useReducer Introduction*~~ [2020-03-01]
- [X] ~~*Reducer Action & State*~~ [2020-03-01]
- [X] ~~*Reducer Action Keys & dispatch*~~ [2020-03-01]
- [X] ~~*Action & State Modification Exercise*~~ [2020-03-01]
- [X] ~~*Action & State Modification Solution*~~ [2020-03-01]
- [X] ~~*React.memo & useCallback*~~ [2020-03-01]

### Context

- [X] ~~*Prop Drilling & Context API*~~ [2020-03-01]
- [X] ~~*Creating a Context Provider*~~ [2020-03-01]
- [ ] Context & useContext Hook
- [ ] Context Practice

### Data Fetching

- [ ] Data Fetching & useEffect Hook
- [ ] Response, Loading, & Error
- [ ] Refactoring to a Custom Hook
- [ ] Refactoring to a Custom Reducer

### Thunks

- [ ] What is a Thunk
- [ ] useThunkReducer Hook
- [ ] Dispatching, Reducers & Hooks
- [ ] Routing & Thunks
- [ ] Implementing Undo & Redo
- [ ] Undo Reducer
- [ ] Redo Reducer Exercise
- [ ] Redo Reducer Solution
- [ ] Managing State in a Form

### Wrapping Up

- [ ] Wrapping Up

## Notes

- Functional version of `this.setState` lends itself to easier unit testing
- Second argument to `this.setState` is a callback that is guaranteed to fire after state has been updated
- `JSON.parse` gets really angry if passed in `undefined` so always check before invoking
- `PropTypes` can't type check `this.state` fields
- Don't put things in `state` that you `can calculate from props`!
- Don't put things in state that you won't use in `render`!
- `useRef` is useful to retain knowledge of previous renders
- `useCallback` returns the same function reference if `dependency array` doesn't change
- `useMemo` returns the same function result if the function props don't change
